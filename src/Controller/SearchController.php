<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AnimalRepository;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function search(AnimalRepository $animRepo, Request $request) //injecte la Request
    {
        //Récupérer sur la request le champ du formulaire avec un get(le name de l'input)
        $search = $animRepo->findByRace($request->get('search'));
        // $request->getBasePath('search');

        //Utiliser la variable obtenue pour faire un findBy en utilisant le repository        

        $user = $this->getUser();
        return $this->render('accueil/index.html.twig', [
            'animals' => $search,
            'user' => $user
        ]);
    }
    
    // public function search(string $searchQ)
    // {
    //     $articles = [];
    //     $connection = ConnectionUtil::getConnection();
    //     $query = $connection->prepare('SELECT * FROM animal WHERE titre LIKE :keywords');
    //     $query->bindValue(':keywords', '%' . $searchQ . '%');
    //     $query->execute();

    //     $results = $query->fetchAll();

    //     if (!$query->rowCount() == 0) {
    //         foreach ($results as $line) {
    //             $articles[] = $this->sqlToBlog($line);
    //         }
    //     } else {
    //         echo "mdr tu sais ce que tu cherche ? parce qu'on dirait pas. ";
    //     }
    //     return $articles;
    // }
}
