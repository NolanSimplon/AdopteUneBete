<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AnimalRepository;
use Doctrine\Common\Persistence\ObjectManager;

class AdminController extends AbstractController
{
    /**
     * @Route("/delAnnounce/{id}", name="delAnnounce")
     */
    public function delAnnounce (ObjectManager $manager, AnimalRepository $repo, int $id) {
        $announce = $repo->find($id);
        $manager->remove($announce);
        $manager->flush();
        return $this->redirectToRoute("accueil");
    }
}
